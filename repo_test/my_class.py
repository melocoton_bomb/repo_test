




class MyClass():
    var1 = None
    var2 = None

    def __init__(self, var1, var2):
        self.var1 = var1
        self.var2 = var2

    def method1(self):
        return 'hello'

    def method2(self):
        return 'world!'

    def _private_method1(self):
        self.var1, self.var2 = self.var2, self.var1

    def __eq__(self, o: object) -> bool:
        return super().__eq__(o)

    def __str__(self) -> str:
        return f'var1={self.var1}, var2={self.var2}'

    def __repr__(self) -> str:
        return self.__str__()

