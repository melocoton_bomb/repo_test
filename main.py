from repo_test.my_class import MyClass

print('Hello world!')

def hola():
	return "hola"
	
print(hola())

myClass = MyClass(1., 2.)

print(myClass.method1())

print(myClass)

myClass._private_method1()

print(myClass)